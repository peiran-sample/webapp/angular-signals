import { Component, inject } from '@angular/core';
import { NgFor } from '@angular/common';

import { CartService } from '../cart.service';
import { CartItemComponent } from "../cart-item/cart-item.component";

@Component({
  selector: 'his-cart-list',
  standalone: true,
  templateUrl: './cart-list.component.html',
  imports: [NgFor, CartItemComponent]
})
export class CartListComponent {
 
  cartService = inject(CartService);
  cartItems = this.cartService.cartItems;

}
