import { Component, computed, inject } from '@angular/core';
import { NgFor, NgClass, NgIf } from '@angular/common';
import { VehicleService } from '../vehicle.service';

@Component({
  selector: 'his-vehicle-list',
  standalone: true,
  imports: [NgClass, NgFor, NgIf],
  templateUrl: './vehicle-list.component.html'
})
export class VehicleListComponent {

  vehicleService = inject(VehicleService);
  pageTitle = 'Vehicles';
  errorMessage = '';

  // Component signals
  vehicles = computed(() => {
    try {
      return this.vehicleService.vehicles();
    } catch (e) {
      this.errorMessage = typeof e === 'string'? e : 'Error';
      return [];
    }
  });
  
  selectedVehicle = this.vehicleService.selectedVehicle;

  // When a vehicle is selected, emit the selected vehicle name
  public onSelected(vehicleName: string): void {
    this.vehicleService.vehicleSelected(vehicleName);
  }

}
